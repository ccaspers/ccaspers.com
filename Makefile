all: public

package-lock.json: package.json
	npm i --package-lock-only

node_modules: package-lock.json
	npm ci
	touch node_modules

lint: node_modules
	npm run lint

public: src
	cp -R src public

clean:
	-rm -rf public

.PHONY: clean lint