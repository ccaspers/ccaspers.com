# ccaspers.com [![Build Status](https://img.shields.io/travis/ccaspers/ccaspers.com/master.svg?style=flat-square)](https://travis-ci.org/ccaspers/ccaspers.com)

- plain js, no libraries or frameworks
- minimal information
- minimal styling
- automatic deployment of master branch via travis-ci
- verified with html5validator and jshint
