/*global Filesystem, Terminal, User*/
(function () {
  "use strict";
  var filesystem = new Filesystem();
  var term = new Terminal(
    "term",
    "(guest@" + document.location.hostname + ") » "
  );
  var user = new User(term);
  document.onkeypress = term.handleKeypress;
  document.onkeydown = term.handleKeypress;
  term.addCommand("screenfetch", function () {
    var result =
      "" +
      "  ____     ____      ___     ____  __ ____     ____  ___  __   ____         \n" +
      " 6MMMMb.  6MMMMb.  6MMMMb   6MMMMb\\`M6MMMMb   6MMMMb `MM 6MM  6MMMMb\\     \n" +
      "6M'   Mb 6M'   Mb 8M'  `Mb MM'    ` MM'  `Mb 6M'  `Mb MM69 \" MM'    `      \n" +
      "MM    `' MM    `'     ,oMM YM.      MM    MM MM    MM MM'    YM.            \n" +
      "MM       MM       ,6MM9'MM  YMMMMb  MM    MM MMMMMMMM MM      YMMMMb        \n" +
      "MM       MM       MM'   MM      `Mb MM    MM MM       MM          `Mb       \n" +
      "YM.   d9 YM.   d9 MM.  ,MM L    ,MM MM.  ,M9 YM    d9 MM     L    ,MM       \n" +
      " YMMMM9   YMMMM9  `YMMM9'YbMYMMMM9  MMYMMM9   YMMMM9 _MM_    MYMMMM9        \n" +
      "                                    MM                                      \n" +
      "                                    MM                                      \n" +
      "                                   _MM_                                     \n" +
      "                                                                            \n" +
      "Welcome, human.                                                             \n" +
      "Type 'help' for a list of available commands.";
    return result;
  });
  term.addCommand("whoami", function () {
    return "ccaspers";
  });

  term.addCommand("info", function () {
    return "programmer, web, java, javascript, python, typescript, cloud, serverless";
  });

  term.addCommand("contact", function () {
    return "mail   : mail@ccaspers.com";
  });

  term.addCommand("ls", function () {
    return filesystem.listFiles().join("\n");
  });

  term.addCommand("open", function (target) {
    var href = filesystem.resolve(target);
    window.open(href);
    return "";
  });

  user.type("screenfetch\n");
  user.wait(800);
  user.type("help # please press enter");
})();
