(function (window) {
  "use strict";

  var PROPAGATE_EVENTS = true;
  var STOP_PROPAGATION = !PROPAGATE_EVENTS;

  var EventEmitter = function () {
    var registry = {};

    function on(event, callback) {
      var callbacks = registry[event] || [];
      callbacks.push(callback);
      registry[event] = callbacks;
    }

    function emit(event, args) {
      var callbacks = registry[event] || [];
      callbacks.map(function (callback) {
        callback(args);
      });
    }

    return {
      on: on,
      emit: emit,
    };
  };

  var Cursor = function (intervallMs) {
    var timeOut;
    var view = document.createElement("span");
    view.textContent = "_";
    view.className = "cursor";

    function stop() {
      clearTimeout(timeOut);
    }

    function blink() {
      timeOut = setTimeout(function () {
        toggleCursor();
        blink();
      }, intervallMs);
    }

    function toggleCursor() {
      view.classList.toggle("hidden");
    }

    function getView() {
      return view;
    }

    return {
      blink: blink,
      getView: getView,
      stop: stop,
    };
  };

  var User = function (terminal, cpm) {
    var queue = [];
    var loopTimeOut;
    var delay = (60 * 1000) / (cpm || 80);

    function quit() {
      clearTimeout(loopTimeOut);
      terminal.terminate();
    }

    function type(text) {
      delay = 100;
      for (var i = 0; i < text.length; i++) {
        queueChar(text[i], delay);
      }
    }

    function queueChar(c, delay) {
      var queueEmpty = queue.length === 0;
      queue.push({
        char: c,
        delay: delay,
      });
      if (queueEmpty) {
        setTimeout(tick, delay);
      }
    }

    function tick() {
      if (queue.length) {
        var next = queue.shift();
        var char = next.char;
        terminal.append(char);
        if (queue.length) {
          var delay = queue[0].delay;
          setTimeout(tick, delay);
        }
      }
    }

    function wait(delay) {
      queueChar("", delay);
    }

    return {
      quit: quit,
      type: type,
      wait: wait,
    };
  };

  var Prompt = function (PS1) {
    PS1 = PS1 || "";
    var buffer = [];
    var emitter = EventEmitter();
    var history = new History();

    function draw() {
      return PS1 + buffer.join("");
    }

    function putChar(char) {
      if (char && char != "\n") {
        buffer.push(char);
      } else {
        var command = buffer.join("");
        history.push(command);
        emitter.emit("execute", command);
        buffer.length = 0;
      }
      emitter.emit("change");
    }

    function delChar() {
      buffer.pop();
      emitter.emit("change");
    }

    function previous() {
      buffer = history.previous().split("");
      emitter.emit("change");
    }

    function next() {
      buffer = history.next().split("");
      emitter.emit("change");
    }

    return {
      draw: draw,
      putChar: putChar,
      delChar: delChar,
      previous: previous,
      next: next,
      on: emitter.on,
    };
  };

  var Terminal = function (cssClass, PS1) {
    var prompt = Prompt(PS1);
    var buffer = "";
    var cursor = Cursor(300);
    var tty = document.getElementsByClassName(cssClass)[0];

    var builtins = {
      help: function () {
        var description =
          "Webterminal of ccaspers. \nYou can issue any of the following commands.\n";
        for (var command in commands) {
          description += "\n- " + command;
        }
        return description;
      },
      nop: function () {
        return 'Command not found.\nType "help" for a list of available commands.';
      },
    };

    var commands = {
      help: builtins.help,
    };

    function addCommand(name, callback) {
      if (typeof callback !== "function") {
        console.log("error, provided callback was not a function");
      }
      commands[name] = callback;
    }

    function draw() {
      tty.textContent = buffer + prompt.draw();
      tty.appendChild(cursor.getView());
    }

    function terminate() {
      cursor.stop();
    }

    function append(text) {
      if (text) {
        scrollDown();
        text.split("").map(function (char) {
          putChar(char);
        });
      }
    }

    function putChar(char) {
      prompt.putChar(char);
    }

    function execute(commandSequence) {
      var commentTokenIndex = commandSequence.indexOf("#");
      if (commentTokenIndex > -1) {
        commandSequence.substring(0, commentTokenIndex);
      }
      var args = commandSequence.split(" ");
      var name = args.shift();
      var command = commands[name] || builtins.nop;
      return command.apply(undefined, args) + "\n\n";
    }

    function scrollDown() {
      var height = tty.scrollHeight;
      if (tty.scrollTop != height) {
        tty.scrollTop = height;
      }
    }

    function handleKeypress(event) {
      console.log(event, event.key, event.keyCode);
      if (event.ctrlKey || event.metaKey) {
        return handleShortcuts();
      } else if (event.key && event.key.length === 1) {
        return handleChar(event);
      } else {
        return handleSpecialKeys(event);
      }
    }

    function handleShortcuts() {
      return PROPAGATE_EVENTS;
    }

    function handleChar(event) {
      var key = event.key;
      if (!key) {
        console.log("legacy browser detected");
        key = String.fromCharCode(event.keyCode) || "";
      } else {
        putChar(key);
        return STOP_PROPAGATION;
      }
    }

    function handleSpecialKeys(event) {
      var key = event.key || lookupKeyCode(event.keyCode) || event.code;
      if (!key) {
        return PROPAGATE_EVENTS;
      }
      switch (key) {
        case "Enter":
          putChar("\n");
          break;
        case "Backspace":
          prompt.delChar();
          break;
        case "ArrowUp":
          prompt.previous();
          break;
        case "ArrowDown":
          prompt.next();
          break;
      }
      return STOP_PROPAGATION;
    }

    function lookupKeyCode(keycode) {
      var keyLut = {
        13: "Enter",
        8: "Backspace",
      };
      return keyLut[keycode];
    }
    /* init */
    cursor.blink();

    tty.appendChild(cursor.getView());

    prompt.on("execute", function (command) {
      buffer += prompt.draw() + "\n";
      buffer += execute(command);
    });

    prompt.on("change", function () {
      draw();
      scrollDown();
    });

    return {
      append: append,
      terminate: terminate,
      addCommand: addCommand,
      handleKeypress: handleKeypress,
    };
  };

  var Filesystem = function () {
    var files = {
      github: "https://www.github.com/ccaspers",
      gitlab: "https://www.gitlab.com/ccaspers",
      linked_in: "https://www.linkedin.com/in/ccaspers/",
    };
    function listFiles() {
      return Object.keys(files);
    }

    function resolve(target) {
      return files[target] || "#";
    }
    return {
      resolve: resolve,
      listFiles: listFiles,
    };
  };

  var History = function () {
    var commands = [];
    var pointer = -1;

    function push(command) {
      console.log("history received command", command);
      commands.push(command);
      pointer = commands.length - 1;
    }

    function previous() {
      var command = commands[pointer];
      pointer = pointer ? pointer - 1 : pointer;
      return command;
    }

    function next() {
      var command = commands[pointer];
      pointer = pointer < commands.length ? pointer + 1 : pointer;
      return command || "";
    }

    function list() {
      return commands.join("\n");
    }

    function clear() {
      commands.length = 0;
      pointer = -1;
    }

    return {
      push: push,
      previous: previous,
      next: next,
      list: list,
      clear: clear,
    };
  };
  window.Filesystem = Filesystem;
  window.Terminal = Terminal;
  window.User = User;
})(window);
